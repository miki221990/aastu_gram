<!DOCTYPE html>
<?php include('server.php') ?>
<html>

<head>
    <meta charset='utf-8'>
    <meta http-equiv='X-UA-Compatible' content='IE=edge'>
    <title>aastugram</title>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <link rel="stylesheet" type="text/css" href="engine1/style.css" />
    <link rel="stylesheet" type="text/css" href="css/home.css" />
    <script type="text/javascript" src="engine1/jquery.js"></script>
    <script type="text/javascript" src="js/main.js"></script>
</head>

<body>
    <div class="home-slide">
        <div id="wowslider-container1">
            <div class="ws_images">
                <ul>
                    <li>
                        <img src="data1/images/10dc9ab571c9b946a87d92ed1179a432.jpg" alt="bootstrap slider" title="" id="wows1_0" />
                    </li>
                    <li><img src="data1/images/7b92602884a7111dbf71801cdaf5f26f.jpg" alt="bootstrap slider" title="7b92602884a7111dbf71801cdaf5f26f" id="wows1_1" /></li>
                </ul>
            </div>
            <div class="ws_bullets">
                <div>
                    <a href="#" title=""><span><img src="data1/tooltips/10dc9ab571c9b946a87d92ed1179a432.jpg" alt=""/>1</span></a>
                    <a href="#" title="7b92602884a7111dbf71801cdaf5f26f"><span><img src="data1/tooltips/7b92602884a7111dbf71801cdaf5f26f.jpg" alt="7b92602884a7111dbf71801cdaf5f26f"/>2</span></a>
                </div>
            </div>
            <div class="ws_script" style="position:absolute;left:-99%"></div>
            <div class="ws_shadow"></div>
        </div>
        <script type="text/javascript" src="engine1/wowslider.js"></script>
        <script type="text/javascript" src="engine1/script.js"></script>
    </div>
    <div class="mini-screen-bg">
        <img src="images/bg3.jpg" alt="small image">
    </div>
    <div class="intro">
        <h1>
            WELCOME
        </h1>
        <p>
            Welome to aastugram,This site is built to connect teachers and students in AASTU.
        </p>
        <div class="input-btn">
                <form action="register.php">
            <input id="sign-up" type="submit" value="Sign Up"  />
                </form>
                <form action="login.php">
            <input id="log-in" type="submit" value="Log In"  />
                </form>
        </div>
    </div>
</body>

</html>