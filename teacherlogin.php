<?php include('server.php') ?>
<!DOCTYPE html>
<html>
<head>
	<title>Login page</title>
	<link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>

	<div class="header">
		<h2>Login</h2>
	</div>
	
	<form method="post" action="login.php">

		<?php include('errors.php'); ?>

		<div class="input-group">
			<label>Username</label>
			<input type="text" name="username" >
		</div>
		<div class="input-group">
			<label>Password</label>
			<input type="password" name="password">
		</div>
		<div class="input-group">
			<button type="submit" class="btn" name="login_user_teacher">Login</button>
		</div>
		<p>
			<p style="color:green;">Not yet a member?</p> <button class="btn"><a href="teacherregister.php">Sign up</a></button>
		</p>
	</form>
    <form action="chooser.php">
<input style="width:30%;background-color:#6495ed;color:white;font-size:20px;border-radius=5px;"
type="submit" value="back to home">
    </form>


</body>
</html>