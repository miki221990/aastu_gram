<?php include('server.php') ?>
<!DOCTYPE html>
<!-- <html>
<head>
	<title>Login page</title>
	<link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>

	<div class="header">
		<h2>Login</h2>
	</div>
	
	<form method="post" action="login.php">

		<?php include('errors.php'); ?>

		<div class="input-group">
			<label>Username</label>
			<input type="text" name="username" >
		</div>
		<div class="input-group">
			<label>Password</label>
			<input type="password" name="password">
		</div>
		<div class="input-group">
			<button type="submit" class="btn" name="login_user">Login</button>
		</div>
		<p>
			<p style="color:gray;">Not yet a member?</p> <button class="btn"><a href="register.php">Sign up</a></button>
		</p>
	</form>
	<form  action="chooser.php">
<input style="width:30%;background-color:#6495ed;color:white;font-size:20px;border-radius=5px;"
type="submit" value="back to home">
    </form>


</body>
</html> -->
<html>

<head>
    <meta charset='utf-8'>
    <meta http-equiv='X-UA-Compatible' content='IE=edge'>
    <title>Log In</title>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <link rel='stylesheet' type='text/css' media='screen' href='css/login.css'>
    <script src='main.js'></script>
</head>

<body>
    <div class="form-body">
        <img src="images/1x/Asset 1.png" alt="aastugramlogo" />
        <h2>LOG IN</h2>
        <div id="form-div">
            <form method="post" action="login.php" autocomplete="on">
				<?php include('errors.php'); ?>
                <label>Email : </label><br/>
                <input class="input-field" type="email" name="email" placeholder="Enter user  Email "><br/>
                <label>Password : </label><br/>
                <input class="input-field" type="password" name="password" placeholder="Enter a Password"><br/>
                <input id="submit-btn" type="submit" name="login_user"value="Log In">
            </form>
        </div>
    </div>


</body>

</html>