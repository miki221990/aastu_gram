<?php include('server.php') ?>
<!DOCTYPE html>
<!-- <html>
<head>
	<title>Registration system PHP and MySQL</title>
	<link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
	<div class="header">
		<h2>Sign Up</h2>
	</div>
	
	<form method="post" action="register.php">

		

		<div class="input-group">
			<label>Username</label>
			<input type="text" name="username"  placeholder="Enter user name">
		</div>
		<div class="input-group">
			<label>Email</label>
			<input type="email" name="email"  placeholder="Enter user name">
		</div>
		<div class="input-group">
			<label>Password</label>
			<input type="password" name="password_1" placeholder="Enter PassWord">
		</div>
		<div class="input-group">
			<label>Confirm password</label>
			<input type="password" name="password_2" placeholder="Confirm passWord">
		</div>
		<div class="input-group">
			<button type="submit" class="btn" name="reg_user">Register</button>
		</div>
		<p>
			<p style="color:blue;">Already a member?</p> <button class="btn" name="log_in"><a href="login.php">Sign in</a></button>
		</p>
	</form>
</body>
</html> -->
<html>

<head>
    <meta charset='utf-8'>
    <meta http-equiv='X-UA-Compatible' content='IE=edge'>
    <title>Sign Up</title>
    <meta name='viewport' content='width=device-width, initial-scale=1.0;'>
    <link rel='stylesheet' type='text/css' media='screen' href='css/signIn.css'>
    <script src='js/signIn.js'></script>
</head>

<body>
    <div class="form-body">
        <img src="images/1x/Asset 1.png" alt="aastugramlogo" />
        <a href="home.html"><img id="cancel" src="images/1x/Cancel.png" /></a>
        <h2>SIGN UP</h2>
        <div id="form-div">
            <form method="post" action="register.php" autocomplete="on">
			<?php include('errors.php'); ?>
                <label>First Name : </label><br/>
                <input class="input-field" type="text" name="username" placeholder="Enter Your First Name"><br/>
                <label>Last Name : </label> <br/>
                <input class="input-field" type="text" name="lastname" placeholder="Enter Your Last Name"><br/>
                <label>Gender : </label>
                <input class="radio-btn" value="Male" type="radio" name="gender-radio" checked><label class="radio-label">Male</label>
                <input class="radio-btn" value="Female"type="radio" name="gender-radio"><label class="radio-label">Female</label><br/>
                <label>Sign Up As : </label>
                <input class="radio-btn2" type="radio" value="Student" name="type-radio" checked><label class="type-label">Student</label>
                <input class="radio-btn2" type="radio" value="Teacher" name="type-radio"><label class="type-label">Teacher</label><br/>
                <label>Email : </label><br/>
                <input class="input-field" type="email" name="email" placeholder="Enter your Email"><br/>
                
                <label>Password : </label><br/>
                <input class="input-field" type="password" name="password_1" placeholder="Enter a Password"><br/>
                <label>Confirm Password : </label><br/>
                <input class="input-field" type="password" name="password_2" placeholder="Confirm Your Password"><br/>
                <input id="submit-btn" type="submit" name="reg_user" value="Sign Up">
            </form>
        </div>
    </div>

</body>

</html>