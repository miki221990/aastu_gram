<?php include('server.php') ?>
<!DOCTYPE html>
<html>
<head>
	<title>Registration page</title>
	<link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
	<div class="header">
		<h2>Sign Up</h2>
	</div>
	
	<form method="post" action="teacherregister.php">

		<?php include('errors.php'); ?>

		<div class="input-group">
			<label>Username</label>
			<input type="text" name="username"  placeholder="Enter user name">
		</div>
		<div class="input-group">
			<label>Email</label>
			<input type="email" name="email"  placeholder="Enter user name">
		</div>
		<div class="input-group">
			<label>Password</label>
			<input type="password" name="password_1" placeholder="Enter PassWord">
		</div>
		<div class="input-group">
			<label>Confirm password</label>
			<input type="password" name="password_2" placeholder="Confirm passWord">
		</div>
		<div class="input-group">
			<button type="submit" class="btn" name="reg_user_teacher">Register</button>
		</div>
		<p>
			<p style="color:blue;">Already a member?</p> <button class="btn" name="log_in"><a href="teacherlogin.php">Sign in</a></button>
		</p>
	</form>
</body>
</html>