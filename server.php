<?php 
	session_start();

	// variable declaration
	$username = "";
	$email    = "";
	$errors = array(); 
	$_SESSION['success'] = "";


	$message="";

	// connect to database
	$db = mysqli_connect("localhost", "root", "root","registration");
	// Check connection
	if (!$db) {
		die("Connection failed: " . mysqli_connect_error());
	}
	//echo "Connected successfully";
	

	// REGISTER USER
	if (isset($_POST['reg_user'])) {
		// receive all input values from the form

		// $username = mysqli_real_escape_string($db, $_POST['username']);
		// $email = mysqli_real_escape_string($db, $_POST['email']);
		// $password_1 = mysqli_real_escape_string($db, $_POST['password_1']);
		// $password_2 = mysqli_real_escape_string($db, $_POST['password_2']);
		$username = $_POST['username'];
		$lastname = $_POST['lastname'];
		$email = $_POST['email'];
		$password_1 =$_POST['password_1'];
		$password_2 =$_POST['password_2'];
		
		$type=$_POST['type-radio'];
		$gender=$_POST['gender-radio'];

		

		// form validation: ensure that the form is correctly filled
		if (empty($username)) { array_push($errors, "Username is required"); }
		if (empty($email)) { array_push($errors, "Email is required"); }
		if (empty($password_1)) { array_push($errors, "Password is required"); }

		if ($password_1 != $password_2) {
			array_push($errors, "The two passwords do not match");
		}

		// register user if there are no errors in the form
		if (count($errors) == 0) {
		//	$password = md5($password_1);//encrypt the password before saving in the database
			if ($_POST['type-radio']=="Student"){
			
			$query = "INSERT INTO users (id,name,lastname,gender, email, password) 
					  VALUES('$email','$username', '$lastname','$gender','$email', '$password_1')";
			mysqli_query($db, $query);

			$_SESSION['username'] = $username;
			$_SESSION['email'] = $email;
		

			//$_SESSION['success'] = "You are now logged in";
			header('location: student.php');
			}
			else {
			
				$query = "INSERT INTO teacherusers (id,name,lastname,gender,email, password) 
				VALUES('$email','$username','$lastname','$gender' ,'$email', '$password_1')";
				mysqli_query($db, $query);

		 $_SESSION['username'] = $username;
		 $_SESSION['email'] = $email;
		
		 //$_SESSION['success'] = "You are now logged in";
		header('location: teacher.php');


			}

		}

	}

	// ... 

	// LOGIN USER
	if (isset($_POST['login_user'])) {
	
		// $username = mysqli_real_escape_string($db, $_POST['username']);
		// $password = mysqli_real_escape_string($db, $_POST['password']);
		$email =  $_POST['email'];
		$password =  $_POST['password'];

		if (empty($email)) {
			array_push($errors, "Username is required");
		}
		if (empty($password)) {
			array_push($errors, "Password is required");
		}
		if (count($errors) == 0) {
			//$password = md5($password);
			$query = "SELECT * FROM users WHERE email='$email' AND password='$password'";
			$result = mysqli_query($db, $query);

			$query1 = "SELECT * FROM teacherusers WHERE email='$email' AND password='$password'";
			$result1 = mysqli_query($db, $query1);

	
        $result=$db->query($query);
        if ($result->num_rows >0){
                while($row=$result->fetch_assoc()){

					
                    
                      if ($row["email"]==$email&&$row["password"]==$password){
						  $_SESSION['username']=$row['name'];
						  $_SESSION['email'] = $row['email'];
						 
						  $_SESSION['success'] = "You are now logged in";
						  header('location: student.php');
						}
					
			
					}

					
				}
				$result1=$db->query($query1);
				  if ($result1->num_rows >0){
                while($row=$result1->fetch_assoc()){
                    
                      if ($row["email"]==$email&&$row["password"]==$password){
						$_SESSION['username']=$row['name'];
						$_SESSION['email'] = $row['email'];
				

						  $_SESSION['success'] = "You are now logged in";
						  header('location: teacher.php');
						}
					
			
					}

					
				}
				

            $db->close();

	
		}

		// if (count($errors) == 0) {
		// 	//$password = md5($password);
		// 	$query = "SELECT * FROM users WHERE name='$username' AND password='$password'";
		// 	$results = mysqli_query($db, $query);

		// 	if (mysqli_num_rows($results) == 1) {
		// 		$_SESSION['username'] = $username;
		// 		$_SESSION['success'] = "You are now logged in";
		// 		header('location: student.php');
		// 	}else {
		// 		array_push($errors, "Wrong username/password combination");
		// 	}
		// }
	}
	//teacher LOGIN USER
	// if (isset($_POST['login_user_teacher'])) {
	
	// 	// $username = mysqli_real_escape_string($db, $_POST['username']);
	// 	// $password = mysqli_real_escape_string($db, $_POST['password']);
	// 	$username =  $_POST['username'];
	// 	$password =  $_POST['password'];

	// 	if (empty($username)) {
	// 		array_push($errors, "Username is required");
	// 	}
	// 	if (empty($password)) {
	// 		array_push($errors, "Password is required");
	// 	}

	// 	if (count($errors) == 0) {
	// 		//$password = md5($password);
	// 		$query = "SELECT * FROM users WHERE name='$username' AND password='$password'";
	// 		$result = mysqli_query($db, $query);

	// 		$query1 = "SELECT * FROM teacherusers WHERE name='$username' AND password='$password'";
	// 		$result1 = mysqli_query($db, $query1);
			
  //       $result=$db->query($query);
  //       if ($result->num_rows >0){
  //               while($row=$result->fetch_assoc()){
                    
  //                     if ($row["name"]==$username&&$row["password"]==$password){
	// 					  $_SESSION['username']=$username;
	// 					  $_SESSION['success'] = "You are now logged in";
	// 					  header('location: student.php');
	// 					}
					
			
	// 				}

					
	// 			}
	// 			$result1=$db->query($query1);
	// 			  if ($result1->num_rows >0){
  //               while($row=$result1->fetch_assoc()){
                    
  //                     if ($row["name"]==$username&&$row["password"]==$password){
	// 					  $_SESSION['username']=$username;
	// 					  $_SESSION['success'] = "You are now logged in";
	// 					  header('location: teacher.php');
	// 					}
					
			
	// 				}

					
	// 			}
				

  //           $db->close();

	
	// 	}
	// }
		// teacher REGISTER USER
		// if (isset($_POST['reg_user_teacher'])) {
		// 	// receive all input values from the form
	
		// 	// $username = mysqli_real_escape_string($db, $_POST['username']);
		// 	// $email = mysqli_real_escape_string($db, $_POST['email']);
		// 	// $password_1 = mysqli_real_escape_string($db, $_POST['password_1']);
		// 	// $password_2 = mysqli_real_escape_string($db, $_POST['password_2']);
		// 	$username = $_POST['username'];
		// 	$email = $_POST['email'];
		// 	$password_1 =$_POST['password_1'];
		// 	$password_2 =$_POST['password_2'];
	
		// 	// form validation: ensure that the form is correctly filled
		// 	if (empty($username)) { array_push($errors, "Username is required"); }
		// 	if (empty($email)) { array_push($errors, "Email is required"); }
		// 	if (empty($password_1)) { array_push($errors, "Password is required"); }
	
		// 	if ($password_1 != $password_2) {
		// 		array_push($errors, "The two passwords do not match");
		// 	}
	
		// 	// register user if there are no errors in the form
		// 	if (count($errors) == 0) {
		// 		//$password = md5($password_1);//encrypt the password before saving in the database
		// 		$query = "INSERT INTO teacherusers (name, email, password) 
		// 				  VALUES('$username', '$email', '$password_1')";
		// 		mysqli_query($db, $query);
	
		// 		$_SESSION['username'] = $username;
		// 		$_SESSION['success'] = "You are now logged in";
		// 		header('location: message.php');
		// 	}
	
		// }
	 

?>