<?php include('server.php') ?>
<?php 
	session_start(); 

	// if (!isset($_SESSION['username'])) {
	// 	$_SESSION['msg'] = "You must log in first";
	// 	header('location: login.php');
	// }

	// if (isset($_GET['logout'])) {
	// 	session_destroy();
	// 	unset($_SESSION['username']);
	// 	header("location: login.php");
	// }

?>

<!DOCTYPE html>
<html>


<head>
    <meta charset='utf-8'>
    <meta http-equiv='X-UA-Compatible' content='IE=edge'>
    <title>Student</title>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <link rel='stylesheet' type='text/css' media='screen' href='css/Student-Style.css'>
    <script src='js/bootstrap.js'></script>
  
</head>

<body>
    <header>
        <div id="logo-section">
            <a href="#"><img id="logo" src="Images/1x/Asset 1.png" alt="User_Img"></a>
            <div class="nav-links">

                <a href="#"><img class="icon" src="Images/1x/Asset 10.png" alt="User_Img"></a>
                <a href="#"><img class="icon" src="Images/1x/Asset 9.png" alt="Announce"></a>
                
                <button type="submit"><img class="icon" src="Images/1x/Asset 8.png" alt="Setting"></button>
                
            </div>
        </div>
    </header>
    <div class="top">
        <img id="profile-pic" src="Images/1x/Asset 6.png" />
        <div class="profile-info">
            <br>
            <!-- <a href="">User name :</a> -->
       
           
            
			<p>Welcome <strong><?php echo $_SESSION['username']?></strong></p>
	
		
            
            <a href="#">Student</a>
           
            <img src="Images/1x/Asset 7.png" />
        
        </div>
        <!-- <div id="post-area">
            <form method="post" action="send.php">
            <textarea name="msg" placeholder="Enter text here"></textarea>
            <input id="post" type="submit" name="post" value="Post">
            </form>
        </div> -->
        <!-- <div class="other-posts">
            <p>Upload attachments </p>
                <form action="upload.php" method="post" enctype="multipart/form-data">
            <input  type="file" name="Filename"  placeholder="upload soft copy">
            <input type="submit" name="upload" value="submit">
                 </form> 
            <a href="#"><img src="Images/1x/Asset 14.png" alt="Img" class="attachment"></a>
            <a href="#"><img src="Images/1x/Asset 13.png" alt="vid" class="attachment"></a>
            <a href="#"><img src="Images/1x/Asset 12.png" alt="pdf" class="attachment"> </a>
            <a href="#"><img src="Images/1x/Asset 11.png" alt="zip" class="attachment"></a>
        </div> -->
    <!-- </div> -->
    <hr color="#111f2e" />
    <div class="tab-justified">
        <table>
            <tr>
                <td><a href="student.php"><button class="tab-link">Students</button></a></td>
                <td><a href="groupsv.php"><button class="tab-link">Groups</button></a></td>
                <td><a href="#"><button class="tab-link">Teachers</button></a></td>
            </tr>
        </table>
    </div>
    <div class="posts">
        <ul>
            <li>
                <!-- <div class="post-profile-pic">
                    <img src="Images/1x/Asset 10.png" alt="" />
                    <p style="align:center;">what the teacher says</p>
                </div> -->
                
                <?php
        // displays chats
        $groupname2=$_SESSION['groupname'];
        $sql="select groupchat.groupname, groupchat.text,groupchat.uname from groupchat inner join teacherusers on groupchat.uname=teacherusers.name";
        $result=$db->query($sql);
        if ($result->num_rows >0){
                while($row=$result->fetch_assoc()){
                 ?>
             <div class="post-content">
              
                 <?php
                   echo  "Department:  ".$row["groupname"].":-"."<br> "."Teacher Name: ".$row['uname'].":"."<br>"."Says->    ".$row["text"]."<br >"."<hr>";
                   ?>
                   </div><br>
                   <?php
            }
        }else {
            echo "No recent chats";
        }

            $db->close();
        ?>
            </li>
        </ul>
    </div>
    <footer>
    <button class="foot-btn"><img src="Images/1x/Asset 5.png" alt="Top" />
            <label>Top</label></button>
            <button class="foot-btn"><img src="Images/1x/Asset 4.png" alt="Setting" />
            <label>Setting</label></button>
            <button class="foot-btn"><img src="Images/1x/Asset 3.png" alt="Help" />
            <label>Help</label></button>
            <form method="post" action="logout.php">
            <button type="submit" name="logout" class="foot-btn"><img src="Images/1x/Asset 2.png" alt="Log Out" />
            <label>Log Out</label></button>
            </form>
            
    </footer>
</body>

</html>